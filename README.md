# avluc.gitlab.io

[![pipeline status](https://gitlab.com/avluc/avluc.gitlab.io/badges/master/pipeline.svg)](https://gitlab.com/avluc/avluc.gitlab.io/commits/master)
[![coverage report](https://gitlab.com/avluc/avluc.gitlab.io/badges/master/coverage.svg)](https://gitlab.com/avluc/avluc.gitlab.io/commits/master)

# Slides
[google-slides](https://docs.google.com/presentation/d/1SmyG3PvTfJgbvoyYhCnlwXXMswrKpWF-AoBmyaBz6bQ/edit?usp=sharing)
